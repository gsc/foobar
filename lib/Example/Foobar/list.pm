package Example::Foobar::list;

=head1 NAME

list - prints all available commands with short descriptions

=cut

use strict;
use warnings;
use Carp;
use File::Basename;
use File::Spec;
use Pod::Usage;

=head1 METHODS

=head2 new

Creates new object and saves a "classpath" for further use.  Classpath
is a list of class name components minus the last one.

=cut

sub new {
    my $class = shift;
    my @classpath = split(/::/, $class);
    pop @classpath;
    bless { classpath => \@classpath }, $class
}

=head2 classpath

Auxiliary method.  Returns classpath as a list.

=cut

sub classpath { @{shift->{classpath}} }

=head2 run

Lists available commands on the stdout.  For each command an attempt is
made to load it, to ensure the module is usable.  If so, its description
is extracted from the NAME section of its pod.

=cut

sub run {
    my $self = shift;
    print "\nAvailable commands are:\n";

    foreach my $mod (sort
		     map {
			 my $name = basename($_);
			 my $filename = File::Spec->catfile($self->classpath,
							    $name);
			 if (exists($INC{$filename})) {
			     ()
			 } else {
			     eval {
				 require $filename;
			     };
		             $name =~ s/\.pm$//;
                             $@ ? () : [$name, $_];
			 }
		     }
		     map {
                           glob File::Spec->catfile($_, $self->classpath,
						    '*.pm')
                     } @INC) {
	my $s;
        open(my $fh, '>', \$s);
        pod2usage(-input => $mod->[1],
                  -output => $fh,
                  -verbose => 99,
	          -sections => ['NAME'],
	          -exitstatus => 'NOEXIT');
        close $fh;
	my (undef,$descr) = split("\n", $s||'');
        unless ($descr) {
            $descr = '    ' . $mod->[0]
        }
	print "$descr\n";
    }        
}

1;

