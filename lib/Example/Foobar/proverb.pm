package Example::Foobar::proverb;

=head1 NAME

proverb - prints a rephrased proverb

=head1 DESCRIPTION

Displays an arbitrary proverb.

=cut

use strict;
use warnings;

my @proverbs = (
    'Scintillate, scintillate, asteroid minikin.',
    'Members of an avian species of identical plumage congregate.',
    'Surveillance should precede saltation.',
    'Pulchritude possesses solely cutaneous profundity.',
    "It is fruitless to become lachrymose over precipitately departed\n"
    ."    lacteal fluid.",
    'Freedom from incrustations of grime is contiguous to rectitude.',
    "It is fruitless to attempt to indoctrinate a superannuated\n"
    ."    canine with innovative maneuvers.",
    'Eschew the implement of correction and vitiate the scion.',
    "The temperature of the aqueous content of an unremittingly\n"
    ."    galled saucepan does not reach 212 degrees Farenheit."
);

=head2 new

Creates a new object and selects a random proverb to print.

=cut

sub new {
    my $class = shift;
    bless { selection => int(rand(@proverbs)) }, $class;
}

=head2 run

Displays the selected proverb on stdout.

=cut

sub run {
    my $self = shift;
    print $proverbs[$self->{selection}],"\n";
}

1;
