package Example::Foobar::limerick;

=head1 NAME

limerick - prints a limerick

=cut

use strict;
use warnings;

=head1 DESCRIPTION

There's hardly any need to comment.  The code is pretty straightforward.
The constructor is trivial.  The B<run> method prints a limerick of
questionable quality and returns.

=cut

sub new {
    bless {}, shift
}

sub run {
    my $self = shift;
    print <<EOT
There was a young man of high station
Who was found by a pious relation
        Making love in a ditch
        To -- I won't say a bitch --
But a woman of no reputation.
EOT
;	
}

1;
