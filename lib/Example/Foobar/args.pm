package Example::Foobar::args;

=head1 NAME

args - prints its arguments

=cut

use strict;
use warnings;

=head1 METHODS

Each loadable module must provide at least two method: the
cosntructor B<new> and runtime method B<run>.

=head2 new

Creates an instance of the class and saves a reference to its
arguments for further use.

=cut

sub new {
    my $class = shift;
    bless { args => \@_ }, $class;
}

=head2 run

Displays the full package name and arguments it's been called with.

=cut

sub run {
    my $self = shift;
    print __PACKAGE__ . " called with arguments "
	  . join(',', @{$self->{args}}) . "\n";
}

1;


