* Overview

This example package illustrates the implementation of dynamically
loaded Perl modules.  The word dynamically here means that the module
name is not known beforehand to the main program.  Instead, it is
supplied by the user via command line or configuration file.

For the clarity, this example implements a simple frontend program
"foobar" that takes as its argument a command name and a list of
arguments for that program.  It looks for a Perl module for the
given command in the Perl path, loads the module and calls its 'run'
methods to perform the actual command.

* Package contents

 foobar
   Command line utility to illustrate command loading
 lib/Example/Foobar.pm
   Command loader.
 lib/Example/Foobar/*.pm
   Example loadable commands.

* Usage

To simplify test usage, the debug.sh script is provided that allows
you to try the package without installing.  Run

  . debug.sh

Then try

  foobar list

This will show you the list of currently implemented commands.  Run
any of them and inspect their sources.

Of course, the list command itself is also implemented as a loadable
module (lib/Example/Foobar/list.pm).

* Further reading

  perldoc lib/Example/Foobar.pm et al.


Local Variables:
mode: outline
paragraph-separate: "[	]*$"
version-control: never
End:


 
